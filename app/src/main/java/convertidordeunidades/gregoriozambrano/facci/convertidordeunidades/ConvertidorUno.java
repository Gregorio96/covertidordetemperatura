package convertidordeunidades.gregoriozambrano.facci.convertidordeunidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConvertidorUno extends AppCompatActivity {


    Button temperatura = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_uno);
        temperatura = (Button)findViewById(R.id.btn1);
        temperatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(), temperatura.class);
                startActivity(cambiar);
            }
        });
    }
}
